import cherrypy
from jinja2 import Environment, FileSystemLoader
from app.controllers import BaseController

class TweetsController(BaseController):

    dance_object = None

    def __init__(self, tweet_store, credentials, update_callback, prefix):
        BaseController.__init__(self)
        self.plugin_name = prefix.split('/')[-1]
        self.env = Environment(loader=FileSystemLoader(prefix.lstrip('/') + '/templates'))
        self.tweet_store = tweet_store
        self.credentials = credentials
        self.update_callback = update_callback

    @cherrypy.expose
    def index(self):
        template = self.env.get_template('index.html')
        tweets = self.tweet_store.all_tweets()
        return template.render(tweets=tweets, name=self.plugin_name)

    @cherrypy.expose
    def settings(self):
        template = self.env.get_template('settings.html')
        return template.render(account=self.credentials.account, name=self.plugin_name)

    @cherrypy.expose
    def reset(self):
        self.credentials.reset()
        self.tweet_store.remove_all()

    @cherrypy.expose
    def dance(self):
        from .lib.twitterdance import TwitterDance
        self.dance_object = TwitterDance(self.credentials.consumer_key, self.credentials.consumer_key_secret)
        result = self.dance_object.launch_pin_browser()
        if not result:
            return self.post_failure_with_errors(['Could not start the music! (a.k.a. open web browser for authentication)'])
        else:
            return self.post_success_with_data('Data')

    @cherrypy.expose
    def pinform(self):
        template = self.env.get_template('pinform.html')
        return template.render(name=self.plugin_name)

    @cherrypy.expose()
    def pinsubmit(self, pincode=''):
        key, secret = self.dance_object.submit_pin(pincode)
        self.credentials.update_tokens(key, secret);
        self.update_callback()