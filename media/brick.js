function twitter_class(name) {

    var thisObject = this;
    this.name = name;
    var elementID = this.name;
    var path = '/plugins/' + elementID;
    var settingsElementId = "twitterSettings";

    this.render = function() {
        dash.render(path + '/', elementID);
    }

    this.settings = function() {
        dash.modalSettings(
            path + '/settings',
            elementID,
            settingsElementId,
            this.exitSettings
        );
    }

    this.reloadSettings = function() {
        dash.reloadSettings(path + '/settings', settingsElementId)
    };

    this.resetUser = function(button) {
        var buttonElement = $(button)
        buttonElement.button('loading')

        $.get(
            path + '/resetUser',
            function() {
                thisObject.reloadSettings();
            }
        );
    }

    this.startDance = function(element) {
        var buttonElement = $(element)
        buttonElement.button('loading')
        $.get(
            path + '/dance',
            function(responseJson) {
                if (responseJson.success == false) {
                    buttonElement.button('reset');
                    for (var error in responseJson.errors) {
                        alert(responseJson.error);
                        return;
                    }
                } else {
                    thisObject.loadPinForm()
                }
            },
            'json'
        );
    };

    this.loadPinForm = function() {
        $.get(
            path + '/pinform',
            function(responseText) {
                $('#twitterDanceButton').replaceWith(responseText);
            }
        );
    };

    this.pinSubmit = function(element) {
        var buttonElement = $(element);
        buttonElement.button('loading');
        $.get(
            path + '/pinsubmit',
            {
                'pincode': $('#twitterSettings > div.modal-body > form > input').val()
            },
            function() {
                thisObject.reloadSettings();
            }
        );
    };

    this.exitSettings = function() {
        this.render()
        dash.hideModalSettings(settingsElementId)

    }
        
};