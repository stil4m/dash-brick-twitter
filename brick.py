
from .lib.updater import Updater
from .lib.tweetstore import TweetStore
from tweetscontroller import TweetsController


from .lib.oauthcredentials import OAuthCredentials

class Brick():

    tweet_store = None
    updater = None
    
    def __init__(self, mapper, prefix, path):
        self.mapper = mapper
        self.prefix = prefix
        self.name = self.prefix.split('/')[-1]
        self.path = path
        self.credentials = OAuthCredentials(self.path + '/config.ini')
        self.tweet_store = TweetStore()
        self.setup_mapper()
    
    def setup_mapper(self):
        if self.prefix and self.mapper:
            tweets_controller = TweetsController(tweet_store=self.tweet_store, credentials=self.credentials, update_callback=self.update, prefix=self.prefix)
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + '/'), controller=tweets_controller, action = 'index')
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + '/settings'), controller=tweets_controller, action = 'settings')
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + '/resetUser'), controller=tweets_controller, action = 'reset')
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + '/dance'), controller=tweets_controller, action = 'dance')
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + '/pinform'), controller=tweets_controller, action = 'pinform')
            self.mapper.connect(name='dash-brick-'+self.name, route=(self.prefix + '/pinsubmit'), controller=tweets_controller, action = 'pinsubmit')

    def update(self):
        if not self.updater:
            self.updater = Updater(self.credentials, self.tweet_store)
        self.updater.execute()

    def get_javascript(self):
        js = "<script type=\"text/javascript\" src=\""+ self.prefix + "/brick.js\"></script>\n"
        js += "<script> var " + self.name + " = new twitter_class('"+ self.name +"')</script>\n"
        js += "<script>" + self.name + ".render()</script>"
        return js

    def timeout(self):
        return 90

    def get_real_name(self):
        return 'dash-brick-twitter'