__author__ = 'mstijlaart'

import time

class SimpleTweet:

    identifier = None
    created_at = None
    user_name = None
    user_screen_name = None
    user_profile_url = None
    retweeter_user_name = None
    retweeter_user_screen_name = None
    text = None

    def __init__(self, tweetData):
        self.identifier = tweetData['id']
        self.created_at = time.strptime(tweetData['created_at'], '%a %b %d %H:%M:%S +0000 %Y')

        if 'retweeted_status' in tweetData:
            self.user_name = tweetData['retweeted_status']['user']['name']
            self.user_screen_name = tweetData['retweeted_status']['user']['screen_name']
            self.retweeter_user_name = tweetData['user']['name']
            self.retweeter_user_screen_name = tweetData['user']['screen_name']
        else:
            self.user_name = tweetData['user']['name']

        self.user_profile_url = tweetData['user']['profile_image_url']
        self.text = tweetData['text']

        if self.retweeter_user_name is not None:
            rep = 'RT @' + self.user_screen_name + ': '
            self.text = self.text.replace(rep, '')

    @property
    def identifier(self):
        return self.identifier

    @property
    def created_at(self):
        return self.created_at

    def __repr__(self):
        return repr((self.user_name, self.user_screen_name, self.text, self.identifier))

