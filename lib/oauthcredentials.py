import ConfigParser
from ..twitterapi.oauth import OAuth
from ..twitterapi.api import Twitter

class OAuthCredentials:

    token = None
    token_secret = None
    consumer_key = None
    consumer_key_secret = None
    account = None

    def __init__(self, config_file_path):
        self.config_file = config_file_path
        self.configParser = ConfigParser.RawConfigParser()
        self.__update_credentials()
        self.verify()

    def __update_credentials(self):
        self.configParser.read(self.config_file)
        self.token = self.__try_to_get_string('credentials', 'token')
        self.token_secret = self.__try_to_get_string('credentials', 'token_secret')
        self.consumer_key = self.__try_to_get_string('credentials', 'consumer_key')
        self.consumer_key_secret = self.__try_to_get_string('credentials', 'consumer_secret')

    def __try_to_get_string(self, section, option):
        try:
            return self.configParser.get(section, option)
        except (ConfigParser.NoOptionError, ConfigParser.NoSectionError):
            pass
        return ''

    def __try_to_set_string(self, section, option, value):
        try:
            self.configParser.set(section, option, value)
        except (ConfigParser.NoOptionError, ConfigParser.NoSectionError):
            pass

    def is_complete(self):
        if self.__validate_credential(self.token) and\
           self.__validate_credential(self.token_secret) and\
           self.__validate_credential(self.consumer_key) and\
           self.__validate_credential(self.consumer_key_secret):
            return True
        else:
            return False

    def verify(self):
        oauth = OAuth(self.token, self.token_secret,
            self.consumer_key, self.consumer_key_secret)
        twitter = Twitter(auth=oauth)
        try:
            self.account = twitter.account.verify_credentials()
        except:
            self.account = None



    def __validate_credential(self, cred):
        if len(cred) is 0:
            return False
        else:
            return True

    def update_file_with_tokens(self):
        self.__try_to_set_string('credentials', 'token', self.token)
        self.__try_to_set_string('credentials', 'token_secret', self.token_secret)
        with open(self.config_file, 'wb') as configfile:
            self.configParser.write(configfile)

    def reset(self):
        self.token = ''
        self.token_secret = ''
        self.update_file_with_tokens()
        self.account = None

    def update_tokens(self, key, secret):
        self.token = key
        self.token_secret = secret
        self.verify()
        if self.account:
            self.update_file_with_tokens()
        else:
            self.token = ''
            self.token_secret = ''

    @property
    def get_account(self):
        return self.account

    @property
    def get_token(self):
        return self.token

    @property
    def get_token_secret(self):
        return self.token_secret

    @property
    def get_consumer_key(self):
        return self.consumer_key

    @property
    def get_consumer_key_secret(self):
        return self.consumer_key_secret
