__author__ = 'mstijlaart'

import ConfigParser
from ..twitterapi.oauth import OAuth
from ..twitterapi.api import Twitter
from localtweet import SimpleTweet

class Updater:

    def __init__(self, credentials, tweet_store):
        self.credentials = credentials
        self.configParser = ConfigParser.RawConfigParser()
        self.tweet_store = tweet_store

    def execute(self):
        if self.credentials.account:
            for tweet in self.__fetch_tweets():
                self.tweet_store.append(SimpleTweet(tweet))

    def __fetch_tweets(self):
        oauth = OAuth(self.credentials.token, self.credentials.token_secret,
            self.credentials.consumer_key, self.credentials.consumer_key_secret)
        twitter = Twitter(auth=oauth)
        last_id = self.tweet_store.last_tweet_id()
        if last_id is 0:
            return twitter.statuses.home_timeline(count=5)
        else:
            return twitter.statuses.home_timeline(count=5, since_id=last_id)