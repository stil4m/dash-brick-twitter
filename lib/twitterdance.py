__author__ = 'mstijlaart'


import webbrowser
import time

from ..twitterapi.api import Twitter
from ..twitterapi.oauth import OAuth

class TwitterDance:

    oauth_token = None
    oauth_token_secret = None

    def __init__(self, consumer_key, consumer_key_secret):
        self.consumer_key = consumer_key
        self.consumer_key_secret = consumer_key_secret

    def launch_pin_browser(self):
        twitter = Twitter(
            auth=OAuth('', '', self.consumer_key, self.consumer_key_secret),
            format='', api_version=None)

        self.oauth_token, \
        self.oauth_token_secret = self.parse_oauth_tokens(
            twitter.oauth.request_token()
        )

        oauth_url = ('http://api.twitter.com/oauth/authorize?oauth_token=' +
                     self.oauth_token)

        try:
            r = webbrowser.open(oauth_url)
            time.sleep(2)
            if not r:
                return False
        except:
            return False
        return True

    def parse_oauth_tokens(self, result):
        for r in result.split('&'):
            k, v = r.split('=')
            if k == 'oauth_token':
                oauth_token = v
            elif k == 'oauth_token_secret':
                oauth_token_secret = v
        return oauth_token, oauth_token_secret

    def submit_pin(self, pincode):
        try:
            twitter = Twitter(
                auth=OAuth(
                    self.oauth_token, self.oauth_token_secret, self.consumer_key, self.consumer_key_secret),
                format='', api_version=None)

            self.oauth_token, self.oauth_token_secret = self.parse_oauth_tokens(
                twitter.oauth.access_token(oauth_verifier=pincode)
            )

            return self.oauth_token, self.oauth_token_secret
        except:
            return '', ''