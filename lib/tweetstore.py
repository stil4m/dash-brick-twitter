__author__ = 'mstijlaart'

class TweetStore:

    tweets = []

    def append(self, new_tweet):
        if self.has_identifier(new_tweet.identifier):
            return

        if len(self.tweets) is 5:
            self.tweets.pop()
        self.tweets.append(new_tweet)
        self.__order_tweets_by_date()

    def __order_tweets_by_date(self):
        self.tweets.sort(key=lambda tweet: tweet.created_at, reverse=True)

    def has_identifier(self, identifier):
        for tweet in self.tweets:
            if tweet.identifier is identifier:
                return True
        return False

    def all_tweets(self):
        return self.tweets

    def last_tweet_id(self):
        if len(self.tweets) is 0:
            return 0
        return self.tweets[0].identifier

    def remove_all(self):
        del self.tweets[:]